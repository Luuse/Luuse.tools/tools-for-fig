<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/themes/luuse-tool/blueprints/resources.yaml',
    'modified' => 1550136228,
    'data' => [
        'title' => 'Resources',
        'extends@' => [
            'type' => 'base',
            'context' => 'blueprints://pages'
        ],
        'form' => [
            'validation' => 'loose',
            'fields' => [
                'tabs' => [
                    'type' => 'tabs',
                    'active' => 1,
                    'fields' => [
                        'advanced' => [
                            'fields' => [
                                'columns' => [
                                    'fields' => [
                                        'column2' => [
                                            'fields' => [
                                                'order_title' => [
                                                    'type' => 'hidden'
                                                ],
                                                'ordering' => [
                                                    'type' => 'hidden',
                                                    'default' => 0,
                                                    'readonly' => true
                                                ],
                                                'order' => [
                                                    'type' => 'hidden'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];
