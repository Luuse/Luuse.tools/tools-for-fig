<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/accounts/marianne.yaml',
    'modified' => 1550142185,
    'data' => [
        'email' => 'salut@etienneozeray.fr',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'fullname' => 'etienne',
        'title' => NULL,
        'state' => 'enabled',
        'hashed_password' => '$2y$10$CMGfvf.rjxqqYt3OrDpLheaPh2EqgrKfJdJSndTnWwCU0Mvwc.Qw.'
    ]
];
