<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/themes/luuse-tool/blueprints/tool.yaml',
    'modified' => 1550051506,
    'data' => [
        'title' => 'Outil',
        'extends@' => [
            'type' => 'base',
            'context' => 'blueprints://pages'
        ],
        'form' => [
            'fields' => [
                'tabs' => [
                    'type' => 'tabs',
                    'active' => 3,
                    'fields' => [
                        'resource' => [
                            'fields' => [
                                'columnsA' => [
                                    'fields' => [
                                        'columnB' => [
                                            'type' => 'column',
                                            'fields' => [
                                                'header.contributor' => [
                                                    'type' => 'text',
                                                    'label' => 'Contributeur',
                                                    'style' => 'vertical'
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                'general' => [
                                    'fields' => [
                                        'columns' => [
                                            'type' => 'columns',
                                            'fields' => [
                                                'column1' => [
                                                    'type' => 'column',
                                                    'fields' => [
                                                        'header.type' => [
                                                            'type' => 'text',
                                                            'label' => 'Type',
                                                            'style' => 'vertical'
                                                        ],
                                                        'header.url' => [
                                                            'type' => 'url',
                                                            'label' => 'Url',
                                                            'style' => 'vertical'
                                                        ],
                                                        'header.gitUrl' => [
                                                            'type' => 'url',
                                                            'label' => 'Git Url',
                                                            'style' => 'vertical'
                                                        ],
                                                        'header.year' => [
                                                            'type' => 'text',
                                                            'label' => 'Année',
                                                            'style' => 'vertical'
                                                        ],
                                                        'header.images' => [
                                                            'type' => 'file',
                                                            'label' => 'Images',
                                                            'style' => 'vertical',
                                                            'destination' => 'self@',
                                                            'multiple' => true,
                                                            'limit' => 4,
                                                            'filesize' => 1.5,
                                                            'accept' => [
                                                                0 => 'image/*'
                                                            ]
                                                        ]
                                                    ]
                                                ],
                                                'column2' => [
                                                    'type' => 'column',
                                                    'fields' => [
                                                        'header.authors' => [
                                                            'btnLabel' => 'Ajouter un auteur',
                                                            'name' => 'authors',
                                                            'type' => 'list',
                                                            'label' => 'Auteur(s)',
                                                            'style' => 'vertical',
                                                            'collapsed' => false,
                                                            'collapsible' => true,
                                                            'multiple' => true,
                                                            'fields' => [
                                                                '.name' => [
                                                                    'type' => 'text',
                                                                    'label' => 'Auteur',
                                                                    'size' => 'medium'
                                                                ],
                                                                '.url' => [
                                                                    'type' => 'url',
                                                                    'label' => 'URL'
                                                                ]
                                                            ]
                                                        ],
                                                        'header.description' => [
                                                            'type' => 'textarea',
                                                            'label' => 'Description',
                                                            'style' => 'vertical'
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                'details' => [
                                    'type' => 'section',
                                    'title' => 'Détails',
                                    'fields' => [
                                        'columns2' => [
                                            'type' => 'columns',
                                            'fields' => [
                                                'column3' => [
                                                    'type' => 'column',
                                                    'fields' => [
                                                        'header.languages' => [
                                                            'label' => 'Langages de programmation (séparés par une virgule)',
                                                            'type' => 'text',
                                                            'style' => 'vertical',
                                                            'size' => 'large'
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'advanced' => [
                            'fields' => [
                                'overrides' => [
                                    'fields' => [
                                        'header.routable' => [
                                            'default' => 0
                                        ],
                                        'header.visible' => [
                                            'default' => 0
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];
