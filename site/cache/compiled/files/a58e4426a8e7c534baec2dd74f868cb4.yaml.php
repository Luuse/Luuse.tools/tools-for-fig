<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/themes/luuse-tool/blueprints.yaml',
    'modified' => 1549965214,
    'data' => [
        'name' => 'Luuse Tool',
        'version' => '0.1.0',
        'description' => 'cms pour rentrer des contenus pour poster.',
        'icon' => 'rebel',
        'author' => [
            'name' => 'luuse',
            'email' => 'contact@luuse.io'
        ],
        'homepage' => 'https://github.com/luuse/grav-theme-luuse-tool',
        'demo' => 'http://demo.yoursite.com',
        'keywords' => 'grav, theme, etc',
        'bugs' => 'https://github.com/luuse/grav-theme-luuse-tool/issues',
        'readme' => 'https://github.com/luuse/grav-theme-luuse-tool/blob/develop/README.md',
        'license' => 'MIT',
        'form' => [
            'validation' => 'loose',
            'fields' => [
                'dropdown.enabled' => [
                    'type' => 'toggle',
                    'label' => 'Dropdown in Menu',
                    'highlight' => 1,
                    'default' => 1,
                    'options' => [
                        1 => 'PLUGIN_ADMIN.ENABLED',
                        0 => 'PLUGIN_ADMIN.DISABLED'
                    ],
                    'validate' => [
                        'type' => 'bool'
                    ]
                ]
            ]
        ]
    ]
];
