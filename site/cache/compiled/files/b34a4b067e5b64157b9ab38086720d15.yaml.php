<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/themes/luuse-tool/blueprints/context.yaml',
    'modified' => 1550170062,
    'data' => [
        'title' => 'Context',
        'extends@' => [
            'type' => 'base',
            'context' => 'blueprints://pages'
        ],
        'form' => [
            'fields' => [
                'tabs' => [
                    'type' => 'tabs',
                    'active' => 3,
                    'fields' => [
                        'resource' => [
                            'fields' => [
                                'general' => [
                                    'fields' => [
                                        'columnsA' => [
                                            'type' => 'columns',
                                            'fields' => [
                                                'columnA' => [
                                                    'type' => 'column',
                                                    'fields' => [
                                                        'header.town' => [
                                                            'type' => 'text',
                                                            'label' => 'Ville',
                                                            'style' => 'vertical'
                                                        ],
                                                        'header.countryCode' => [
                                                            'type' => 'text',
                                                            'label' => 'Code Pays',
                                                            'style' => 'vertical'
                                                        ],
                                                        'header.date' => [
                                                            'type' => 'date',
                                                            'label' => 'Date',
                                                            'style' => 'vertical'
                                                        ]
                                                    ]
                                                ],
                                                'columnB' => [
                                                    'type' => 'column',
                                                    'fields' => [
                                                        'header.description' => [
                                                            'type' => 'textarea',
                                                            'label' => 'Description',
                                                            'style' => 'vertical'
                                                        ],
                                                        'header.participants' => [
                                                            'type' => 'textarea',
                                                            'label' => 'Participants',
                                                            'style' => 'vertical'
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ],
                        'advanced' => [
                            'fields' => [
                                'columns' => [
                                    'fields' => [
                                        'column2' => [
                                            'fields' => [
                                                'order_title' => [
                                                    'type' => 'hidden'
                                                ],
                                                'ordering' => [
                                                    'type' => 'hidden',
                                                    'default' => 0,
                                                    'readonly' => true
                                                ],
                                                'order' => [
                                                    'type' => 'hidden'
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
];
