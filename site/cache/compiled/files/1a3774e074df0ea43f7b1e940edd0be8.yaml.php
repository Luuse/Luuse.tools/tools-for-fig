<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/plugins/padtocss/blueprints.yaml',
    'modified' => 1550141152,
    'data' => [
        'name' => 'Padtocss',
        'version' => '0.1.0',
        'description' => 'Pad',
        'icon' => 'plug',
        'author' => [
            'name' => 'Luuse',
            'email' => 'contact@luuse.io'
        ],
        'homepage' => 'https://github.com/luuse/grav-plugin-padtocss',
        'demo' => 'http://demo.yoursite.com',
        'keywords' => 'grav, plugin, etc',
        'bugs' => 'https://github.com/luuse/grav-plugin-padtocss/issues',
        'docs' => 'https://github.com/luuse/grav-plugin-padtocss/blob/develop/README.md',
        'license' => 'MIT',
        'form' => [
            'validation' => 'strict',
            'fields' => [
                'enabled' => [
                    'type' => 'toggle',
                    'label' => 'PLUGIN_ADMIN.PLUGIN_STATUS',
                    'highlight' => 1,
                    'default' => 0,
                    'options' => [
                        1 => 'PLUGIN_ADMIN.ENABLED',
                        0 => 'PLUGIN_ADMIN.DISABLED'
                    ],
                    'validate' => [
                        'type' => 'bool'
                    ]
                ],
                'padUrl' => [
                    'type' => 'text',
                    'label' => 'Pad Url'
                ],
                'posterRoute' => [
                    'type' => 'pages',
                    'size' => 'medium',
                    'classes' => 'fancy',
                    'label' => 'Poster Page',
                    'show_all' => false,
                    'show_modular' => false,
                    'show_root' => false,
                    'help' => 'PLUGIN_ADMIN.HOME_PAGE_HELP'
                ],
                'historyMode' => [
                    'type' => 'toggle',
                    'label' => 'History Mode ?',
                    'highlight' => 1,
                    'default' => 0,
                    'options' => [
                        1 => 'PLUGIN_ADMIN.ENABLED',
                        0 => 'PLUGIN_ADMIN.DISABLED'
                    ]
                ],
                'historyRoute' => [
                    'type' => 'pages',
                    'size' => 'medium',
                    'classes' => 'fancy',
                    'label' => 'History Page',
                    'show_all' => false,
                    'show_modular' => false,
                    'show_root' => false,
                    'help' => 'PLUGIN_ADMIN.HOME_PAGE_HELP'
                ]
            ]
        ]
    ]
];
