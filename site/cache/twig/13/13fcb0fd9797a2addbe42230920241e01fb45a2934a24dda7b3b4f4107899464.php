<?php

/* partials/base.html.twig */
class __TwigTemplate_e2605bc4f884de1c2debcc97ac51a5dc521c94e7d8e947ad67852e4d274fdb63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'header' => [$this, 'block_header'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["theme_config"] = $this->getAttribute($this->getAttribute(($context["config"] ?? null), "themes", []), $this->getAttribute($this->getAttribute($this->getAttribute(($context["config"] ?? null), "system", []), "pages", []), "theme", []));
        // line 2
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 3
        echo (($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", []), "getActive", [])) ? ($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", []), "getActive", [])) : ($this->getAttribute(($context["theme_config"] ?? null), "default_lang", [])));
        echo "\">
<head>
";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 21
        echo "</head>
<body class=\"";
        // line 22
        echo $this->getAttribute(($context["page"] ?? null), "template", []);
        echo "\">
";
        // line 23
        $this->displayBlock('header', $context, $blocks);
        // line 25
        $this->displayBlock('body', $context, $blocks);
        // line 27
        $this->displayBlock('javascripts', $context, $blocks);
        // line 30
        echo $this->getAttribute(($context["assets"] ?? null), "js", [], "method");
        echo "

</body>
</html>
";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        // line 6
        echo "    <meta charset=\"utf-8\" />
    <title>";
        // line 7
        if ($this->getAttribute(($context["header"] ?? null), "title", [])) {
            echo twig_escape_filter($this->env, $this->getAttribute(($context["header"] ?? null), "title", []), "html");
            echo " | ";
        }
        echo twig_escape_filter($this->env, $this->getAttribute(($context["site"] ?? null), "title", []), "html");
        echo "</title>
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    ";
        // line 10
        $this->loadTemplate("partials/metadata.html.twig", "partials/base.html.twig", 10)->display($context);
        // line 11
        echo "    ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        echo ($context["theme_url"] ?? null);
        echo "/assets/css/print.css\" media=\"print\">
    ";
        // line 18
        echo $this->getAttribute(($context["assets"] ?? null), "css", [], "method");
        echo "
    ";
        // line 19
        echo $this->env->getExtension('Grav\Plugin\PadTwigExtension')->padToCss();
        echo "
";
    }

    // line 11
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 12
        echo "        ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", [0 => "theme://assets/css/main.css", 1 => 100], "method");
        // line 13
        echo "        ";
        if ((($this->getAttribute(($context["page"] ?? null), "template", []) == "poster") && ($this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", []), "route", []) == $this->getAttribute($this->getAttribute($this->getAttribute(($context["config"] ?? null), "plugins", []), "padtocss", []), "historyRoute", [])))) {
            // line 14
            echo "            <link rel=\"stylesheet\" type=\"text/css\" href=\"/2019/01-fig/tools-for-fig/site/user/pages/historique/";
            echo $this->getAttribute(($context["page"] ?? null), "folder", []);
            echo "/pad.css\">
        ";
        }
        // line 16
        echo "    ";
    }

    // line 23
    public function block_header($context, array $blocks = [])
    {
    }

    // line 25
    public function block_body($context, array $blocks = [])
    {
    }

    // line 27
    public function block_javascripts($context, array $blocks = [])
    {
        // line 28
        echo "    ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", [0 => "theme://assets/js/main.js", 1 => 100], "method");
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 28,  123 => 27,  118 => 25,  113 => 23,  109 => 16,  103 => 14,  100 => 13,  97 => 12,  94 => 11,  88 => 19,  84 => 18,  79 => 17,  76 => 11,  74 => 10,  64 => 7,  61 => 6,  58 => 5,  49 => 30,  47 => 27,  45 => 25,  43 => 23,  39 => 22,  36 => 21,  34 => 5,  29 => 3,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set theme_config = attribute(config.themes, config.system.pages.theme) %}
<!DOCTYPE html>
<html lang=\"{{ grav.language.getActive ?: theme_config.default_lang }}\">
<head>
{% block head %}
    <meta charset=\"utf-8\" />
    <title>{% if header.title %}{{ header.title|e('html') }} | {% endif %}{{ site.title|e('html') }}</title>
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    {% include 'partials/metadata.html.twig' %}
    {% block stylesheets %}
        {% do assets.addCss('theme://assets/css/main.css', 100) %}
        {% if page.template == \"poster\" and page.parent.route == config.plugins.padtocss.historyRoute %}
            <link rel=\"stylesheet\" type=\"text/css\" href=\"/2019/01-fig/tools-for-fig/site/user/pages/historique/{{ page.folder }}/pad.css\">
        {% endif %}
    {% endblock %}
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ theme_url }}/assets/css/print.css\" media=\"print\">
    {{ assets.css() }}
    {{ loadPad() }}
{% endblock head %}
</head>
<body class=\"{{ page.template }}\">
{% block header %}
{% endblock %}
{% block body %}
{% endblock %}
{% block javascripts %}
    {% do assets.addJs('theme://assets/js/main.js', 100) %}
{% endblock %}
{{ assets.js() }}

</body>
</html>
", "partials/base.html.twig", "/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/themes/luuse-tool/templates/partials/base.html.twig");
    }
}
