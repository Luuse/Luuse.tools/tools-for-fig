<?php

/* error.html.twig */
class __TwigTemplate_00f833833026273312d185c94ea9b7f638f43f5feaedc87a795b8655a3684f76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("partials/base.html.twig", "error.html.twig", 1);
        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        // line 4
        echo "    <div class=\"lead text-center\">
        <h1>Error!</h1>
        ";
        // line 6
        echo $this->getAttribute(($context["page"] ?? null), "content", []);
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'partials/base.html.twig' %}

{% block content %}
    <div class=\"lead text-center\">
        <h1>Error!</h1>
        {{ page.content }}
    </div>
{% endblock %}
", "error.html.twig", "/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/themes/luuse-tool/templates/error.html.twig");
    }
}
