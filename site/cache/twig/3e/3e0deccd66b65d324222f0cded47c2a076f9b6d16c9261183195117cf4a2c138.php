<?php

/* history.html.twig */
class __TwigTemplate_97cf43dc29b72dc7f16127a42711c86f942c0d0b6aca33f444be28462af0b292 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("partials/base.html.twig", "history.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = [])
    {
        // line 3
        echo "<main>
\t<section class=\"versions\">
\t\t";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, $this->getAttribute(($context["page"] ?? null), "children", [])));
        foreach ($context['_seq'] as $context["_key"] => $context["poster"]) {
            // line 6
            echo "\t\t\t<a href=\"";
            echo $this->getAttribute($context["poster"], "url", []);
            echo "\">
\t\t\t\t";
            // line 7
            echo twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["poster"], "header", []), "timestamp", []), "d.m.y, G.i");
            echo " - ";
            echo $this->getAttribute($this->getAttribute($context["poster"], "header", []), "commit", []);
            echo " - ";
            echo $this->getAttribute($this->getAttribute($context["poster"], "header", []), "person", []);
            echo "
\t\t\t</a>
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['poster'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        echo "\t</section>
</main>
";
    }

    public function getTemplateName()
    {
        return "history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 10,  44 => 7,  39 => 6,  35 => 5,  31 => 3,  28 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'partials/base.html.twig' %}
{% block body %}
<main>
\t<section class=\"versions\">
\t\t{% for poster in page.children|reverse %}
\t\t\t<a href=\"{{ poster.url }}\">
\t\t\t\t{{ poster.header.timestamp|date('d.m.y, G.i') }} - {{ poster.header.commit }} - {{ poster.header.person }}
\t\t\t</a>
\t\t{% endfor %}
\t</section>
</main>
{% endblock %}", "history.html.twig", "/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/themes/luuse-tool/templates/history.html.twig");
    }
}
