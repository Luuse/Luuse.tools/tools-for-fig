<?php

/* poster.html.twig */
class __TwigTemplate_045a57cb54dcd33831bb316196dbf0c3aeba392c0f30aedab7e1f86fabb0b831 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("partials/base.html.twig", "poster.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "partials/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["about"] = $this->getAttribute(($context["pages"] ?? null), "find", [0 => "/fig-festival"], "method");
        // line 3
        $context["path"] = ((($this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", []), "slug", []) != "historique")) ? ("") : ("../"));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        // line 5
        echo "<main>
\t<section id=\"poster\">
\t\t<section class=\"resources\">
\t\t\t";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["page"] ?? null), "children", []));
        foreach ($context['_seq'] as $context["_key"] => $context["resource"]) {
            // line 9
            echo "\t\t\t<div class=\"resource ";
            echo $this->getAttribute($context["resource"], "template", []);
            echo " ";
            echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("hyphen", $this->getAttribute($this->getAttribute($context["resource"], "header", []), "contributor", []));
            echo "\" id=\"";
            echo $this->getAttribute($context["resource"], "slug", []);
            echo "\">
\t\t\t\t<header>
\t\t\t\t\t<h2>";
            // line 11
            echo $this->getAttribute($this->getAttribute($context["resource"], "header", []), "title", []);
            echo "</h2>
\t\t\t\t\t";
            // line 12
            if (((($this->getAttribute($this->getAttribute($context["resource"], "header", []), "type", []) || $this->getAttribute($this->getAttribute($context["resource"], "header", []), "year", [])) || $this->getAttribute($this->getAttribute($context["resource"], "header", []), "url", [])) || $this->getAttribute($this->getAttribute($context["resource"], "header", []), "authors", []))) {
                // line 13
                echo "\t\t\t\t\t\t<ul class=\"general\">
\t\t\t\t\t\t\t";
                // line 14
                if ($this->getAttribute($this->getAttribute($context["resource"], "header", []), "type", [])) {
                    echo "<li class=\"type\">";
                    echo $this->getAttribute($this->getAttribute($context["resource"], "header", []), "type", []);
                    echo "</li>";
                }
                // line 15
                echo "\t\t\t\t\t\t\t";
                if ($this->getAttribute($this->getAttribute($context["resource"], "header", []), "year", [])) {
                    echo "<li class=\"year\">";
                    echo $this->getAttribute($this->getAttribute($context["resource"], "header", []), "year", []);
                    echo "</li>";
                }
                // line 16
                echo "\t\t\t\t\t\t\t";
                if ($this->getAttribute($this->getAttribute($context["resource"], "header", []), "url", [])) {
                    echo "<li class=\"url\"><a href=\"";
                    echo $this->getAttribute($this->getAttribute($context["resource"], "header", []), "url", []);
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute($context["resource"], "header", []), "url", []);
                    echo "</a></li>";
                }
                // line 17
                echo "\t\t\t\t\t\t\t";
                if ($this->getAttribute($this->getAttribute($context["resource"], "header", []), "authors", [])) {
                    // line 18
                    echo "\t\t\t\t\t\t\t\t<ul class=\"authors\">
\t\t\t\t\t\t\t\t\t";
                    // line 19
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["resource"], "header", []), "authors", []));
                    foreach ($context['_seq'] as $context["_key"] => $context["author"]) {
                        // line 20
                        echo "\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
                        echo $this->getAttribute($context["author"], "url", []);
                        echo "\">";
                        echo $this->getAttribute($context["author"], "name", []);
                        echo "</a></li>
\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['author'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 22
                    echo "\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t";
                }
                // line 24
                echo "\t\t\t\t\t\t</ul>
\t\t\t\t\t";
            }
            // line 26
            echo "\t\t\t\t</header>
\t\t\t\t";
            // line 27
            if ($this->getAttribute($this->getAttribute($context["resource"], "header", []), "images", [])) {
                // line 28
                echo "\t\t\t\t<div class=\"images\">
\t\t\t\t\t";
                // line 29
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["resource"], "header", []), "images", []));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 30
                    echo "\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t";
                    // line 31
                    $context["imgPath"] = ((($this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", []), "route", []) == $this->getAttribute($this->getAttribute($this->getAttribute(($context["config"] ?? null), "plugins", []), "padtocss", []), "historyRoute", []))) ? (((((((("../user/pages/" . $this->getAttribute($this->getAttribute(($context["page"] ?? null), "parent", []), "folder", [])) . "/") . $this->getAttribute(($context["page"] ?? null), "folder", [])) . "/") . $this->getAttribute($context["resource"], "folder", [])) . "/") . $this->getAttribute($context["image"], "name", []))) : (((((("user/pages/" . $this->getAttribute(($context["page"] ?? null), "folder", [])) . "/") . $this->getAttribute($context["resource"], "folder", [])) . "/") . $this->getAttribute($context["image"], "name", []))));
                    // line 32
                    echo "\t\t\t\t\t\t\t<img src=\"";
                    echo ($context["imgPath"] ?? null);
                    echo "\">
\t\t\t\t\t\t</div>
\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 35
                echo "\t\t\t\t</div>
\t\t\t\t";
            }
            // line 37
            echo "\t\t\t</div>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['resource'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "\t\t</section>
\t\t<section id=\"colophon\">
\t\t\t<h1>";
        // line 41
        echo $this->getAttribute($this->getAttribute(($context["about"] ?? null), "header", []), "title", []);
        echo "</h1>
\t\t\t<h2>";
        // line 42
        echo twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["about"] ?? null), "header", []), "date", []), "d.m.y");
        echo "</h2>
\t\t\t<h3>";
        // line 43
        echo twig_trim_filter($this->getAttribute($this->getAttribute(($context["about"] ?? null), "header", []), "town", []));
        echo ", ";
        echo $this->getAttribute($this->getAttribute(($context["about"] ?? null), "header", []), "countryCode", []);
        echo "</h3>
\t\t\t<p>Avec ";
        // line 44
        echo $this->getAttribute($this->getAttribute(($context["about"] ?? null), "header", []), "participants", []);
        echo "</p>
\t\t</section>
\t</section>
\t";
        // line 47
        if ($this->getAttribute(($context["page"] ?? null), "home", [])) {
            // line 48
            echo "\t<section id=\"pad\">
\t\t<iframe name=\"embed_readwrite\" src=\"";
            // line 49
            echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["config"] ?? null), "plugins", []), "padtocss", []), "padUrl", []);
            echo "?showControls=true&showChat=false&showLineNumbers=true&useMonospaceFont=true\"></iframe>
\t</section>
\t<section id=\"buttons\">
\t\t";
            // line 52
            if ($this->getAttribute($this->getAttribute($this->getAttribute(($context["config"] ?? null), "plugins", []), "padtocss", []), "historyMode", [])) {
                // line 53
                echo "\t\t\t<div class=\"button\" id=\"backup\">Back Up</div>
\t\t\t<form method=\"POST\" action=\"";
                // line 54
                echo $this->getAttribute(($context["page"] ?? null), "url", []);
                echo $this->getAttribute($this->getAttribute($this->getAttribute(($context["config"] ?? null), "plugins", []), "padtocss", []), "historyRoute", []);
                echo "\" class=\"hidden\">
\t\t\t\t<div>
\t\t\t\t\t<div>
\t\t\t\t\t\t<label for=\"person\">Person</label>
\t\t\t\t\t\t<input type=\"text\" name=\"person\" id=\"person\">
\t\t\t\t\t</div>
\t\t\t\t\t<div>
\t\t\t\t\t\t<label for=\"person\">Commit</label>
\t\t\t\t\t\t<textarea name=\"commit\" id=\"commit\"></textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<input type=\"number\" name=\"timestamp\" value=\"";
                // line 65
                echo twig_date_format_filter($this->env, "now", "U");
                echo "\" class=\"hidden\">
\t\t\t\t<input type=\"submit\" class=\"button\" value=\"Save\" id=\"save\">
\t\t\t</form>
\t\t";
            }
            // line 69
            echo "\t\t<div class=\"button\" id=\"print\">Print</div>
\t\t<div class=\"button\" id=\"refresh\">Refresh</div>
\t</section>
\t";
        }
        // line 73
        echo "</main>
";
    }

    public function getTemplateName()
    {
        return "poster.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 73,  215 => 69,  208 => 65,  193 => 54,  190 => 53,  188 => 52,  182 => 49,  179 => 48,  177 => 47,  171 => 44,  165 => 43,  161 => 42,  157 => 41,  153 => 39,  146 => 37,  142 => 35,  132 => 32,  130 => 31,  127 => 30,  123 => 29,  120 => 28,  118 => 27,  115 => 26,  111 => 24,  107 => 22,  96 => 20,  92 => 19,  89 => 18,  86 => 17,  77 => 16,  70 => 15,  64 => 14,  61 => 13,  59 => 12,  55 => 11,  45 => 9,  41 => 8,  36 => 5,  33 => 4,  29 => 1,  27 => 3,  25 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'partials/base.html.twig' %}
{% set about = pages.find(\"/fig-festival\") %}
{% set path = (page.parent.slug != \"historique\") ? \"\" : \"../\" %}
{% block body %}
<main>
\t<section id=\"poster\">
\t\t<section class=\"resources\">
\t\t\t{% for resource in page.children %}
\t\t\t<div class=\"resource {{ resource.template }} {{ resource.header.contributor|hyphenize }}\" id=\"{{ resource.slug }}\">
\t\t\t\t<header>
\t\t\t\t\t<h2>{{ resource.header.title }}</h2>
\t\t\t\t\t{% if resource.header.type or resource.header.year or resource.header.url or resource.header.authors %}
\t\t\t\t\t\t<ul class=\"general\">
\t\t\t\t\t\t\t{% if resource.header.type %}<li class=\"type\">{{ resource.header.type }}</li>{% endif %}
\t\t\t\t\t\t\t{% if resource.header.year %}<li class=\"year\">{{ resource.header.year }}</li>{% endif %}
\t\t\t\t\t\t\t{% if resource.header.url %}<li class=\"url\"><a href=\"{{ resource.header.url }}\">{{ resource.header.url }}</a></li>{% endif %}
\t\t\t\t\t\t\t{% if resource.header.authors %}
\t\t\t\t\t\t\t\t<ul class=\"authors\">
\t\t\t\t\t\t\t\t\t{% for author in resource.header.authors %}
\t\t\t\t\t\t\t\t\t\t<li><a href=\"{{ author.url }}\">{{ author.name }}</a></li>
\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t</ul>
\t\t\t\t\t{% endif %}
\t\t\t\t</header>
\t\t\t\t{% if resource.header.images %}
\t\t\t\t<div class=\"images\">
\t\t\t\t\t{% for image in resource.header.images %}
\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t{% set imgPath = (page.parent.route == config.plugins.padtocss.historyRoute) ? \"../user/pages/\" ~ page.parent.folder  ~ \"/\"  ~ page.folder  ~ \"/\"  ~ resource.folder  ~ \"/\"  ~ image.name : \"user/pages/\" ~ page.folder  ~ \"/\"  ~ resource.folder  ~ \"/\"  ~ image.name  %}
\t\t\t\t\t\t\t<img src=\"{{ imgPath }}\">
\t\t\t\t\t\t</div>
\t\t\t\t\t{% endfor %}
\t\t\t\t</div>
\t\t\t\t{% endif %}
\t\t\t</div>
\t\t\t{% endfor %}
\t\t</section>
\t\t<section id=\"colophon\">
\t\t\t<h1>{{ about.header.title }}</h1>
\t\t\t<h2>{{ about.header.date|date('d.m.y') }}</h2>
\t\t\t<h3>{{ about.header.town|trim }}, {{ about.header.countryCode }}</h3>
\t\t\t<p>Avec {{ about.header.participants }}</p>
\t\t</section>
\t</section>
\t{% if page.home %}
\t<section id=\"pad\">
\t\t<iframe name=\"embed_readwrite\" src=\"{{ config.plugins.padtocss.padUrl }}?showControls=true&showChat=false&showLineNumbers=true&useMonospaceFont=true\"></iframe>
\t</section>
\t<section id=\"buttons\">
\t\t{% if config.plugins.padtocss.historyMode %}
\t\t\t<div class=\"button\" id=\"backup\">Back Up</div>
\t\t\t<form method=\"POST\" action=\"{{ page.url }}{{ config.plugins.padtocss.historyRoute }}\" class=\"hidden\">
\t\t\t\t<div>
\t\t\t\t\t<div>
\t\t\t\t\t\t<label for=\"person\">Person</label>
\t\t\t\t\t\t<input type=\"text\" name=\"person\" id=\"person\">
\t\t\t\t\t</div>
\t\t\t\t\t<div>
\t\t\t\t\t\t<label for=\"person\">Commit</label>
\t\t\t\t\t\t<textarea name=\"commit\" id=\"commit\"></textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<input type=\"number\" name=\"timestamp\" value=\"{{ 'now'|date('U') }}\" class=\"hidden\">
\t\t\t\t<input type=\"submit\" class=\"button\" value=\"Save\" id=\"save\">
\t\t\t</form>
\t\t{% endif %}
\t\t<div class=\"button\" id=\"print\">Print</div>
\t\t<div class=\"button\" id=\"refresh\">Refresh</div>
\t</section>
\t{% endif %}
</main>
{% endblock %}", "poster.html.twig", "/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/themes/luuse-tool/templates/poster.html.twig");
    }
}
