<?php

/* languages.yaml.twig */
class __TwigTemplate_dd29ac64d02b36bfeb1a5ee781f1410ab44cceaa88200ffe8e638fe756a5d136 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "en:
  PLUGIN_";
        // line 2
        echo twig_upper_filter($this->env, $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("underscor", $this->getAttribute(($context["component"] ?? null), "name", [])));
        echo ":
    TEXT_VARIABLE: Text Variable
    TEXT_VARIABLE_HELP: Text to add to the top of a page
";
    }

    public function getTemplateName()
    {
        return "languages.yaml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("en:
  PLUGIN_{{ component.name|underscorize|upper }}:
    TEXT_VARIABLE: Text Variable
    TEXT_VARIABLE_HELP: Text to add to the top of a page
", "languages.yaml.twig", "/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/plugins/devtools/components/plugin/blank/languages.yaml.twig");
    }
}
