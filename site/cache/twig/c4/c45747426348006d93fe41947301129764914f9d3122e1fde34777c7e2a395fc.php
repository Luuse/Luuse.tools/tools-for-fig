<?php

/* blueprints.yaml.twig */
class __TwigTemplate_1f03b6b1e493eb7091cc5921e59f5444a07ce63293f8183dffe47b652268023b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "name: ";
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("title", $this->getAttribute(($context["component"] ?? null), "name", []));
        echo "
version: 0.1.0
description: ";
        // line 3
        echo $this->getAttribute(($context["component"] ?? null), "description", []);
        echo "
icon: plug
author:
  name: ";
        // line 6
        echo $this->getAttribute($this->getAttribute(($context["component"] ?? null), "author", []), "name", []);
        echo "
  email: ";
        // line 7
        echo $this->getAttribute($this->getAttribute(($context["component"] ?? null), "author", []), "email", []);
        echo "
homepage: https://github.com/";
        // line 8
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("hyphen", $this->getAttribute($this->getAttribute(($context["component"] ?? null), "author", []), "githubid", []));
        echo "/grav-plugin-";
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("hyphen", $this->getAttribute(($context["component"] ?? null), "name", []));
        echo "
demo: http://demo.yoursite.com
keywords: grav, plugin, etc
bugs: https://github.com/";
        // line 11
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("hyphen", $this->getAttribute($this->getAttribute(($context["component"] ?? null), "author", []), "githubid", []));
        echo "/grav-plugin-";
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("hyphen", $this->getAttribute(($context["component"] ?? null), "name", []));
        echo "/issues
docs: https://github.com/";
        // line 12
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("hyphen", $this->getAttribute($this->getAttribute(($context["component"] ?? null), "author", []), "githubid", []));
        echo "/grav-plugin-";
        echo $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("hyphen", $this->getAttribute(($context["component"] ?? null), "name", []));
        echo "/blob/develop/README.md
license: MIT

form:
  validation: strict
  fields:
    enabled:
      type: toggle
      label: PLUGIN_ADMIN.PLUGIN_STATUS
      highlight: 1
      default: 0
      options:
        1: PLUGIN_ADMIN.ENABLED
        0: PLUGIN_ADMIN.DISABLED
      validate:
        type: bool
    text_var:
      type: text
      label: PLUGIN_";
        // line 30
        echo twig_upper_filter($this->env, $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("underscor", $this->getAttribute(($context["component"] ?? null), "name", [])));
        echo ".TEXT_VARIABLE
      help: PLUGIN_";
        // line 31
        echo twig_upper_filter($this->env, $this->env->getExtension('Grav\Common\Twig\TwigExtension')->inflectorFilter("underscor", $this->getAttribute(($context["component"] ?? null), "name", [])));
        echo ".TEXT_VARIABLE_HELP
";
    }

    public function getTemplateName()
    {
        return "blueprints.yaml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 31,  76 => 30,  53 => 12,  47 => 11,  39 => 8,  35 => 7,  31 => 6,  25 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("name: {{ component.name|titleize }}
version: 0.1.0
description: {{ component.description }}
icon: plug
author:
  name: {{ component.author.name }}
  email: {{ component.author.email }}
homepage: https://github.com/{{ component.author.githubid|hyphenize }}/grav-plugin-{{ component.name|hyphenize }}
demo: http://demo.yoursite.com
keywords: grav, plugin, etc
bugs: https://github.com/{{ component.author.githubid|hyphenize }}/grav-plugin-{{ component.name|hyphenize }}/issues
docs: https://github.com/{{ component.author.githubid|hyphenize }}/grav-plugin-{{ component.name|hyphenize }}/blob/develop/README.md
license: MIT

form:
  validation: strict
  fields:
    enabled:
      type: toggle
      label: PLUGIN_ADMIN.PLUGIN_STATUS
      highlight: 1
      default: 0
      options:
        1: PLUGIN_ADMIN.ENABLED
        0: PLUGIN_ADMIN.DISABLED
      validate:
        type: bool
    text_var:
      type: text
      label: PLUGIN_{{ component.name|underscorize|upper }}.TEXT_VARIABLE
      help: PLUGIN_{{ component.name|underscorize|upper }}.TEXT_VARIABLE_HELP
", "blueprints.yaml.twig", "/Users/planomarianne/Documents/Bureau/taff/2019/01-fig/tools-for-fig/site/user/plugins/devtools/components/plugin/blank/blueprints.yaml.twig");
    }
}
