<?php
namespace Grav\Plugin;

use \Grav\Common\Plugin;


class LoadHistory extends \Twig_Extension{

    public function __construct($first){

        $this->css = $first;
    }

    public function getName(){
        return 'LoadHistory';
    }
    
    public function getFunctions(){
        
        return [

            new \Twig_SimpleFunction('loadHistory', [$this, 'loadHistory'])
        ];
    }

    public function loadHistory(){
            
            return "bisous"; 
    }
}