---
title: '1962 Versioned physical sculptures'
contributor: Marianne
type: Installation
url: 'http://raphaelbastide.com/1962'
year: '2017'
description: '1962 Sculptures conceptualized using a revision control system and represented physically. Any physical interpretation of the PIECE documentation hosted on this GitHub repository, or a versions of it will be part of the 1962 project.'
date: '16-02-2019 00:00'
routable: false
visible: false
images:
    user/pages/02.ressources/1962-versioned-physical-sculptures/0_9_3.jpg:
        name: 0_9_3.jpg
        type: image/jpeg
        size: 258963
        path: user/pages/02.ressources/1962-versioned-physical-sculptures/0_9_3.jpg
    user/pages/02.ressources/1962-versioned-physical-sculptures/0_9_revisable-1.jpg:
        name: 0_9_revisable-1.jpg
        type: image/jpeg
        size: 145242
        path: user/pages/02.ressources/1962-versioned-physical-sculptures/0_9_revisable-1.jpg
media_order: '0_9_3.jpg,ARC-MUND-AFF-PS-EC1938-09_CT.JPG'
contributeur: 'Julie Noire'
tools: 'marteau, corde, mur'
custom_file:
    user/themes/luuse-tool/images/0_9_3.jpg:
        name: 0_9_3.jpg
        type: image/jpeg
        size: 258963
        path: user/themes/luuse-tool/images/0_9_3.jpg
    user/themes/luuse-tool/images/0_9_revisable-1.jpg:
        name: 0_9_revisable-1.jpg
        type: image/jpeg
        size: 145242
        path: user/themes/luuse-tool/images/0_9_revisable-1.jpg
group: 'GRAPHIC WORK'
author:
    -
        text: 'Raphael Bastide'
auto:
    -
        text: 'Raphael Bastide'
    -
        text: 'Jean-Jérôme Fourchette'
auteur:
    -
        text: 'Raphael Bastide'
myimage: 'ARC-MUND-AFF-PS-EC1938-09_CT.JPG,0_9_3.jpg,0_9_3.jpg'
text: '1962 Versioned physical sculptures'
buttons:
    -
        url: ''
        date: ''
        group: 'GRAPHIC WORK'
        custom_file: null
---

