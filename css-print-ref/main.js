function threeFig(num){

	if(num < 10){

		return "00"+num;

	}else if(num < 100){

		return "0"+num;

	}else{

		return num;
	}
}

function menu(){

	var rond = document.getElementById("rond");
	var sous = document.querySelector(".sous-content");
	var sommaire = document.getElementById("sommaire");

	rond.addEventListener("mouseenter", function(){

		if(!sommaire.classList.contains("fixed")){

			sommaire.classList.add("fixed");
		}
	});

	sous.addEventListener("mouseleave", function(){

		if(sommaire.classList.contains("fixed")){

			sommaire.classList.remove("fixed");
		}
	});
}

function setName(board, i){

	var name = "board-"+threeFig(i);

	board.dataset.board=name;

}

function scrollToNext(index, boards){

	var max = boards.length - 1;
	var next = (index !== max) ? index+1 : 0;
	var nextDiv = document.querySelector("[data-board='board-"+threeFig(next)+"']");

	nextDiv.scrollIntoView();
}

function powerPoint(){

	var boards = document.querySelectorAll(".sous-content h1, .sous-content p, .sous-content ul:not(#sommaire)>li, .sous-content .image, #ref");

	boards.forEach(function(board, i){

		setName(board, i);

		board.addEventListener("click", function(){

			scrollToNext(i, boards);

		});

	});
}

menu();
powerPoint();
