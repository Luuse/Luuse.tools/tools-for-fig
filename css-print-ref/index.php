<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Présentation Éditions hybrides</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="hello">Bonjour !</div>
  <div id="content">
      <?php include("dessin.php") ?>
      <div id="rond">⎘</div>
      <ul id="sommaire">
        <br>
        <a href="#content">⇡⬆↑⇯↥↿⇑⤒  </a><br>
        <li> <a href="#web-print"> Web et print ?</a></li>
        <li> <a href="#cssprint">CSS print </a></li>
        <li> <a href="#media">@media query</a></li>
        <li> <a href="#input-output">Input ⟷ Output</a></li>
        <li> ⟶ <a href="#forme">Multi-format </a></li>
        <li>&nbsp;&nbsp;&nbsp;&nbsp;⌒◝◞<a href="#drulhe"> Louise Drulhe </a><li>
        <li>&nbsp;&nbsp;&nbsp;&nbsp;⌒◝◞<a href="#lavillahermosa"> La Villa Hermosa </a><li>
        <li> ⟶ <a href="#tools">Tools </a></li>
        <li>&nbsp;&nbsp;&nbsp;&nbsp;⌒◝◞<a href="#osp"> OSP</a><li>
        <li>&nbsp;&nbsp;&nbsp;&nbsp;⌒◝◞<a href="#ytp"> Luuse </a><li>
        <li>⟶  <a href="#version">Versions </a></li>
        <li>&nbsp;&nbsp;&nbsp;&nbsp;⌒◝◞<a href="#bastide">1962, Raphael Bastide </a><li>
        <li>&nbsp;&nbsp;&nbsp;&nbsp;⌒◝◞<a href="#ppp">PPP, Raphael Bastide </a><li>
        <li>⟶  <a href="#collaboration">Collaboratif </a></li>
          <li>&nbsp;&nbsp;&nbsp;&nbsp;⌒◝◞<a href="#luuse">DEViation,  Luuse</a><li>
          <li>&nbsp;&nbsp;&nbsp;&nbsp;⌒◝◞<a href="#poisson">Poisson Évêque, Luuse </a><li><br>
        <li> <a href="#ref">References / liens </a></li>
      </ul>
      <section class="sous-content">
        <h1 id="web-print"> Pourquoi utiliser des outils Web pour faire du print ? </h1>
          <ul>
            <li> Changer la logique de mise en page conventionnelle sur un logiciel où chaque page succède à l’autre de manière narrative, fonctionnelle et efficace. </li>
            <li> Intéragir à plusieur sur un même document et possiblement en temps réel.</li>
              <li> Envisager des technologies plus conviviales. </li>
              <li> Se permettre de faire évoluer et adapter un outil à chaque projet.</li>
              <li> Permettre une entrée unique de contenu avec multiplicité de format en sortie.</li>
              <li> Avoir des systèmes et outils où l’aspect génératif et/ou structurel diffère et peut apporter des problématiques supplémentaires potentiellement bénéfiques à la pratique.</li>
              <li> Interroger l’utilisation de ses outils de mise en page, de manière théorique et technique, pour en avoir une conscience poussée.</li>
              <li> Développer son autonomie et sa propre méthode de travail pour proposer des objets atypiques et variés.</li>
            </ul>
          <h1 id="cssprint"> Comment ça marche ? </h1>
          <p class="citation">« Le même contenu a deux formes différentes mais complémentaires, produites par le même outil&nbsp;: un navigateur web, connecté à internet d’un côté, et capable de générer un PDF de l’autre. Il n’y a pas la nécessité d’utiliser plusieurs logiciels, d’effectuer des opérations complexes pour passer d’une version “numérique” à un fichier imprimable, mais simplement du logiciel le plus utilisé&nbsp;: un navigateur web. »
            <br><br><u class="note"> <a href="http://strabic.fr" "target=_blank"> Workshop PrePostPrint Chercher, manipuler, partager, imprimer, strabic, Antoine Fauchié, 29 juin 2017</a> </u></p>
          <h1>Techniquement<br>
            css / html / navigateur.
<h1>Structure et style
<code><pre>
<_HTML="structure_Sémantique"/>

CSS{
  ⟶ Style;
}
</pre>
</code>
</h1>
<h1 id="media">@media
<code><pre>
@media print{
  @page{
  size:21cm 29.7cm;
  }
}
</pre>
</code>
</h1>
<h1 id="input-output">Input ⟷ Output</h1>
<p>Un contenu en entrée. <br>⇵<br>  Plusieurs formats et/ou médias en sortie.</p>

            <div class="image">
                <img src="img/0.png">
                <h2><a href="https://prepostprint.org/doku.php//fr/introduction" target="_blank"> Schéma réalisé par Raphael Bastide </a></h2>
            </div>
            <div class="image">
              <img src="img/00.png" alt="">
              <h2><a href="http://design-research.be/hybrid/publications.html" target="_blank">Schéma réalisé par Loraine Furter</a></h2>
            </div>
            <div class="image">
              <img src="img/11.png" alt="">
              <h2><a href="http://design-research.be/hybrid/publications.html" target="_blank">Schéma réalisé par Loraine Furter</a></h2>
            </div>
            <div class="image verysmall">
              <img src="img/10.png" alt="">
              <h2><a href="http://design-research.be/hybrid/publications.html" target="_blank">Schéma réalisé par Loraine Furter</a></h2>
            </div>
            <h1 id="forme">Multi-formats</h1>
            <div id="drulhe"  class="image small">
              <img src="img/1.png" alt="">
              <h2><a href="http://louisedrulhe.fr/lab/#diplome" target="_blank">The Critical Atlas of Internet, Louise Drulhe</a></h2>
            </div>
            <div class="image">
              <img src="img/3.png" alt="">
              <h2><a href="http://louisedrulhe.fr/lab/#diplome" target="_blank">The Critical Atlas of Internet, Louise Drulhe  </a></h2>
            </div>
            <div class="image">
              <img src="img/5.png" alt="">
              <h2><a href="http://louisedrulhe.fr/lab/#diplome"  target="_blank">The Critical Atlas of Internet, Louise Drulhe </a></h2>
            </div>
            <div class="image small">
              <img src="img/6.png" alt="">
              <h2><a href="http://louisedrulhe.fr/lab/#diplome"  target="_blank">The Critical Atlas of Internet, Louise Drulhe</a></h2>
            </div>
            <div id="lavillahermosa" class="image">
              <img src="img/16.png" alt="">
              <h2><a href="http://blog.lavillahermosa.com/brass-%e2%86%92-print-tool-v1/" target="_blank">Print-tool-v1, Villa Hermosa </a>  </h2>
            </div>
            <div class="image">
              <iframe src="https://player.vimeo.com/video/123732089" width="640" height="360" frameborder="0" allowfullscreen></iframe>
              <h2><a href="http://blog.lavillahermosa.com/brass-%e2%86%92-print-tool-v1/" target="_blank">Print-tool-v1, Villa Hermosa </a>  </h2>
          </div>
          <h1 id="tools"> Outils : <br><br>
            <h1> objet réflexif, qui participe et influence la manière de penser et réaliser un projet.</h1>

            <h1>Créer un dialogue designer/machine, avoir une approche active face à notre outillage. Se réapproprier et se constituer ses outils adaptés.</h1>
          <div id="osp" class="image">
            <img src="img/13.png" alt="">
            <h2><a href="http://osp.kitchen/tools/html2print" target="_blank"> html2print, osp </a></h2>
          </div>
          <div class="image">
            <img src="img/14.png" alt="">
            <h2><a href="http://osp.kitchen/tools/html2print" target="_blank"> html2print, osp </a></h2>
          </div>
          <div class="image">
            <img src="img/15.png" alt="">
            <h2><a href="http://osp.kitchen/tools/html2print" target="_blank"> html2print, osp </a></h2>
          </div>
          <div id="ytp" class="image">
            <img src="img/08.png" alt="">
            <h2><a href="http://www.luuse.io/provisoire/#yah2p" target="_blank"> yah2p, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/02.png" alt="">
            <h2><a href="http://www.luuse.io/provisoire/#yah2p" target="_blank"> yah2p, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/06.png" alt="">
            <h2><a href="http://www.luuse.io/provisoire/#yah2p" target="_blank"> yah2p, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/12.JPG" alt="">
            <h2><a href="http://www.luuse.io/provisoire/#yah2p" target="_blank"> yah2p, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/11.JPG" alt="">
            <h2><a href="http://www.luuse.io/provisoire/#yah2p" target="_blank"> yah2p, Luuse </a>  </h2>
          </div>

            <h1 id="version"> Versions : <br><br>
              Différents états, différentes variantes d'un objet. Cela correspond à un état donné de l'évolution d'un fichier à un moment donné.</h1>

              <h1>En graphisme ou en programmation, il est pratique d'utiliser «git» pour travailler à plusieurs ou revenir sur des versions antérieur.
            </h1>
             <div class="image">
               <img src="img/git1.gif" alt="">
               <h2><a href="https://www.miximum.fr/blog/enfin-comprendre-git/" target="_blank">Enfin comprendre git, Thibault Jouannic </a></h2>
              </div>

            <div id="bastide" class="image small">
              <img src="img/12.png" alt="">
              <h2><a href="https://raphaelbastide.com/1962/" target="_blank">1962, Raphael Bastide </a>  </h2>
              </div>
              <div class="image verysmall">
                <img src="img/revisable.jpg" alt="">
                <h2><a href="https://raphaelbastide.com/1962/" target="_blank">1962, Raphael Bastide </a>  </h2>
                </div>
              <div id="ppp" class="image small">
                <img src="img/32.png" alt="">
                <h2><a href="https://prepostprint.org/parsons/" target="_blank">PPP flyer, Raphaël Bastide </a>  </h2>
              </div>
              <div class="image small">
                <img src="img/31.png" alt="">
                <h2><a href="https://prepostprint.org/parsons/" target="_blank">PPP flyer, Raphaël Bastide </a>  </h2>
              </div>
              <div class="image verysmall">
                <img src="img/21.jpg" alt="">
                <h2><a href="https://prepostprint.org/parsons/"  target="_blank">PPP flyer, Raphaël Bastide </a></h2>
              </div>

          <h1 id="collaboration"> Collaboratif :<br><br>
              <h1>  Permet de créer des objet à plusieurs, en étant en même temps sur le même documents.</h1>
              <h1>  Donne la possibilité de travailler de manière asynchrone.</h1>
              <h1>  Un «pad» par exemple donne la possibilité d'écrire des textes à plusieurs.</h1>

          </h1>
          <div id="luuse" class="image">
            <img src="img/22.jpg" alt="">
            <h2><a href="https://gitlab.com/atelier-bek/prepostprint" target="_blank"> DEViation, Luuse</a>  </h2>
          </div>
          <div class="image small">
            <img src="img/23.jpg" alt="">
            <h2><a href="https://gitlab.com/atelier-bek/prepostprint" target="_blank"> DEViation, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/9.jpg" alt="">
            <h2><a href="https://gitlab.com/atelier-bek/prepostprint" target="_blank"> DEViation, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/10.jpg" alt="">
            <h2><a href="https://gitlab.com/atelier-bek/prepostprint" target="_blank"> DEViation, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/11.jpg" alt="">
            <h2><a href="https://gitlab.com/atelier-bek/prepostprint" target="_blank"> DEViation, Luuse </a>  </h2>
          </div>
          <div class="image">
            <video controls width="250">
              <source src="img/video-derivation.mp4" type="video/mp4">
            </video>
            <video src="img/video-derivatio.mp4">
            <h2><a href="https://gitlab.com/atelier-bek/poisson-eveque" target="_blank"> DEViation, Luuse </a>  </h2>
          </div>

          <div id="poisson" class="image small">
            <img src="img/08-poisson.png" alt="">
            <h2><a href="https://gitlab.com/atelier-bek/poisson-eveque" target="_blank"> Poisson-Évêque, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/07-POISSON.png" alt="">
            <h2><a href="https://gitlab.com/atelier-bek/poisson-eveque" target="_blank"> Poisson-Évêque, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/12-poisson_eveque-couv.png" alt="">
            <h2><a href="https://gitlab.com/atelier-bek/poisson-eveque" target="_blank"> Poisson-Évêque, Luuse </a>  </h2>
          </div>
          <div class="image small">
            <img src="img/13-poisson_eveque.png" alt="">
            <h2><a href="https://gitlab.com/atelier-bek/poisson-eveque" target="_blank"> Poisson-Évêque, Luuse </a>  </h2>
          </div>



        </section>
        <section id="ref">
          <ul class="box"> ⌒◝◞ ⌒◝◞&nbsp; ⌒◝◞⌒◝◞&nbsp;⌒◝◞&nbsp;DES OBJETS ↴&nbsp; ⌒◝◞⌒&nbsp; ⌒◝◞⌒◝◞&nbsp; <◝◞&nbsp; <br><br>
            <li>⌒◝◞&nbsp;<a href="https://medor.coop/fr/"target="_blank"><u>Médor </u></a></li>
            <li>⌒◝◞&nbsp;<a href="https://timrodenbroeker.github.io/lofi-poster-machine/" target="_blank"><u>LoFi Poster Machine by Tim Rodenbr&#246;ker </u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://www.editions-hyx.com/fr/code-x"target="_blank"><u>Code X </u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://internet-atlas.net/"target="_blank"><u>Critical Atlas of Internet</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://internet-atlas.net/order"target="_blank"><u>Critical Atlas of Internet</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://conversations.tools"target="_blank"><u>I think that conversations are the best, biggest thing that free software has to offer its user</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://www.balmoral.club.gbodywork"target="_blank"><u>G.BODYWORK</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://blog.lavillahermosa.com/brass&#x25;E2&#x25;86&#x25;92printtoolv1"target="_blank"><u>La Villa Hermosa &quot; BRASS &#8594; Print tool v1</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://htmloutput.risd.gd"target="_blank"><u>for  with  in</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://johncaserta.com/webtoprint.html" target="_blank"><u>John Caserta Web to Print</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://louisedrulhe.fr/designfluide/#sommaire" target="_blank"><u>Design fluide</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://design.lavillahermosa.com/works-75-can-you-dig-it"target="_blank"><u>Can you dig it</u></a></li>
            <li>⌒◝◞&nbsp;<a href="https://copy-this-book.eu/?fbclid=IwAR3n7k8LHNzjQhzpK3_KEFfgMmcrPXUsYgltdnUVoBy8m5p4Rz_HadsJl7I#news"target="_blank"><u>Copy This Book. An Artist&#8217;s Guide to Copyright</u></a></li>
            <li>⌒◝◞&nbsp;<a href=" http://www.theradiohistorian.org/Radiofax/newspaper_of_the_air1.htm"target="_blank"><u>The Newspaper of the Air</u></a></li>
            <li>⌒◝◞&nbsp;<a href="https://furter.github.io/public-domain/"target="_blank"><u>Design the public domain</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://138.68.93.192/"target="_blank"><u>Print Capsule</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://drawingcurved.osp.kitchen/foreword.xhtml"target="_blank"><u>Drawing Curved</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://albertinemeunier.net/dadaprint3r"target="_blank"><u>DadaPrint3r</u></a></li>
            <li>⌒◝◞&nbsp;<a href="https://centerforfuturepublishing.wordpress.com/projets-de-recherche/automatic-publishing/"target="_blank"><u>Omnirama: journal algorithmique</u></a></li>
            <li>⌒◝◞&nbsp;<a href=" https://centerforfuturepublishing.wordpress.com/projets-de-recherche/self-assembling-book/"target="_blank"><u>Self-Assembling Book</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://projects.robertoarista.it/posts/inhabitations/"target="_blank"><u>Inhabitations</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://projects.robertoarista.it/posts/VLM/"target="_blank"><u>VLM</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://osp.kitchen/work/balsamine.2018-2019"target="_blank"><u>La Balsamine 2018-2019</u></a></li>
           <li>⌒◝◞&nbsp;<a href="http://osp.kitchen/work/balsamine.2017-2018"target="_blank"><u>La Balsamine 2017-2018</u></a></li>
           <li>⌒◝◞&nbsp;<a href="http://osp.kitchen/work/balsamine.2011-2012"target="_blank"><u>La Balsamine 2011-2012</u></a></li>
           <li>⌒◝◞&nbsp;<a href="http://osp.kitchen/work/balsamine.2016-2017"target="_blank"><u>La Balsamine 2016-2017</u></a></li>
           <li>⌒◝◞&nbsp;<a href="http://osp.kitchen/work/balsamine.2014-2015"target="_blank"><u>La Balsamine 2014-2015</u></a></li>
           <li>⌒◝◞&nbsp;<a href="http://osp.kitchen/work/balsamine.2012-2013"target="_blank"><u>La Balsamine 2012-2013</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="https://www.nytimes.com/2008/04/14/business/media/14link.html"target="_blank"><u>Phillip Parker</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://www.le-tigre.net/"target="_blank"><uLe Tigre</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://f-u-t-u-r-e.org/"target="_blank"><u>&lt;o&gt; future &lt;o&gt; </u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://www.anonymous-press.com/"target="_blank"><u>Anonymous press</u></a></li>
           <li>⌒◝◞&nbsp;<a  href=" https://raphaelbastide.com/"target="_blank"><u>Rapha&#235;l Bastide</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://blog.lavillahermosa.com/homeopape-nuit-blanche/"target="_blank"><u>Homepape</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="https://fathominfonotebook1908"target="_blank"><u>Frankenfont</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://www.felixheyes.com/Google-Book"target="_blank"><u>Google Book</u></a></li>
           <li>⌒◝◞&nbsp;<a  href=" http://www.books.constantvzw.org/fr/home/death_of_the_authors"target="_blank"><u>The death of the author</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://www.lulu.com/shop/constant/the-death-of-the-authors-james-joyce-rabindranath-tagore-their-return-to-life-in-four-seasons/paperback/product-21310596.html"target="_blank"><u>The death of the author</u></a></li>

           <li>⌒◝◞&nbsp;<a  href="http://www.cnap-n.fr/generateur/index.php"target="_blank"><u>Cnap catalogue</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://novel.coryarcangel.com/"target="_blank"><u>Working On My Novel</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://kabk.github.io/govt-theses-15/ "target="_blank"><u>Eric Schrijver - Hybrid Publishing Back To The Future Publishing Theses at the KABK</u></u></a></li>

           <li>⌒◝◞&nbsp;<a  href="https://lorainefurter.net/fr/"target="_blank"><u>Loraine Furter</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://deconstructionconference.nl/ "target="_blank"><u>deconstruction<</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="https://eracom.github.io/workshop-outils-hybrides-2018/ "target="_blank"><u>Workshop outils d'édition hybrides</u></a></li>


           <li>⌒◝◞&nbsp;<a  href="http://www.evan-roth.com/work/internet-cache-self-portrait/"target="_blank"><u>Internet Cache Self Portrait series</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://www.luuse.io/provisoire/#fimpah"target="_blank"><u>villa Noailles FIMPAH</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://www.luuse.io/provisoire/#domesticpools "target="_blank"><u>villa Noailles Domestic pools</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://www.luuse.io/provisoire/#dp"target="_blank"><u>villa Noailles Design Parade</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://www.luuse.io/provisoire/#pitchouns"target="_blank"><u>villa Noailles Pitchouns</u></a></li>
           <li>⌒◝◞&nbsp;<a  href="http://www.luuse.io/provisoire/#constant"target="_blank"><u>Constant flyers</u></a></li>
         <li>⌒◝◞&nbsp;<a  href="http://design.lavillahermosa.com/works-324-le-geste-radiophonique-audiographie-dun-atelier "target="_blank"><u>Le geste radiophonique</u></a></li>



          </ul>
          <ul class="box">⌒⌒◝DES RÉFÉRENCES TECHNIQUES ET DES OUTILS ↴&nbsp;⌒◝◞<br><br>
             <li>⌒◝◞&nbsp;<a href="https://www.print-css.rocks" target="blank"><u>css print </u></a></li>
             <li>⌒◝◞&nbsp;<a href="http://vladocar.github.io/Hartija---CSS-Print-Framework/hartija.html" target="blank"><u>css print frameworks </u></a></li>
             <li>⌒◝◞&nbsp;<a href="http://osp.kitchen/tools/html2print/" target="blank"><u>Html2print </u></a></li>
             <li>⌒◝◞&nbsp;<a href="https://www.pagedmedia.org/paged-js/" target="blank"><u>paged.js </u></a></li>
             <li>⌒◝◞&nbsp;<a href="https://github.com/bastianallgeier/letter" target="blank"><u>Letter </u></a></li>
             <li>⌒◝◞&nbsp;<a href="https://xxyxyz.org/even/" target="blank"><u>Even </u></a></li>
             <li>⌒◝◞&nbsp;<a href="https://github.com/bachy/libriis" target="blank"><u>Libris </u></a></li>
             <li>⌒◝◞&nbsp;<a href="https://evanbrooks.info/bindery/" target="blank"><u>Bindery.js </u></a></li>
             <li>⌒◝◞&nbsp;<a href=" https://weasyprint.org/" target="blank"><u>weasyprint </u></a></li>
             <li>⌒◝◞&nbsp;<a href="http://basiljs.ch/about/" target="blank"><u>Basiljs </u></a></li>
             <li>⌒◝◞&nbsp;<a href="https://gitlab.com/Luuse/Luuse.tools/web2paper" target="blank"><u>Yah2p </u></a></li>
             <li>⌒◝◞&nbsp;<a href="http://www.luuse.io/provisoire/#yah2p" target="blank"><u>Yah2p </u></a></li>
             <li>⌒◝◞&nbsp;<a href="https://www.scribus.net/" target="blank"><u>Scribus </u></a></li>
             <li>⌒◝◞&nbsp;<a href="http://www.luuse.io/provisoire/#pad2print" target="blank"><u>pad2print </u></a>
             <li>⌒◝◞&nbsp;<a href="https://gitlab.com/Luuse/pad2print" target="blank"><u>pad2print </u></a></li>
             <li>⌒◝◞&nbsp;<a href="http://osp.kitchen/tools/ethertoff/" target="blank"><u>Ethertoff </u></a></li>
             <li>⌒◝◞&nbsp;<a href="https://smashingmagazine.com/2015/01/designing-for-print-with-css/" target="blank"><u>Print With CSS </u></a></li>
          </ul>

          <ul class="box"> DES RÉFÉRENCES THÉORIQUES ↴ <br><br>
            <li>⌒◝◞&nbsp;<a href="https://strabic.fr/Workshop-PrePostPrint" target="blank"><u> Chercher, manipuler, partager, imprimer, Antoine Fauchié  </u></a></li>
            <li>⌒◝◞&nbsp;<a href="https://www.cairn.info/revue-sciences-du-design-2018-2-page-45.htm#" target="blank"><u> Repenser les chaînes de publication par l’intégration des pratiques du développement logiciel, Antoine Fauchié et Thomas Parisot </u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://recherche.julie-blanc.fr/timeline-publishing/" target="blank"><u>Timeline of technologies for publishing (1963-2018), Julie Blanc  </u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://recherche.julie-blanc.fr/revealjs/20180820_Lure.html" target="blank"><u> Paginer le flux, Julie Blanc  </u></a></li>
          </ul>
          <ul class="box">  DES ARCHIVES, DES GROUPES OU DES INITIATIVES ↴ </br><br>
            <li>⌒◝◞&nbsp;<a href="http://pagedmedia.org/" target="blank"><u>Paged Media </u></a></li>
            <li>⌒◝◞&nbsp;<a href="https://prepostprint.org" target="blank"><u>PrePostPrint</u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://computedlayout.tumblr.com/" target="blank"><u>Computed Layout </u></a></li>
            <li>⌒◝◞&nbsp;<a href="http://p-dpa.net/" target="blank"><u>Post—digital publishing archive </u></a></li>
            <li>⌒◝◞&nbsp;<a href=" http://libregraphicsmag.com/" target="blank">><u>Libre Graphics</u></a></li>
          </ul>
        </section>
    </body>
    <script type="text/javascript" src="main.js"></script>
    </html>
