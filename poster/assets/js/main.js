function ajax(receiver){

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange=function() {
    if (this.readyState == 4 && this.status == 200) {
      receiver.innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "load.php");
  xhttp.send();

}

function refresh(){

	var receiver = document.getElementById("style");
	var button = document.getElementById("refresh");
  document.onkeypress = function(evt) {
    evt = evt || window.event;
    var charCode = evt.keyCode || evt.which;
    if (charCode == '114') {
      ajax(receiver);

    }
};

	button.addEventListener("click", function(){

		ajax(receiver);

	});
}


$(document).ready(function() {
  $("#pad").resizable({
    handleSelector: ".splitter",
    resizeHeight: false,
    invert: true
  });
  $("#poster, #colophon").resizable({
    handleSelector: ".splitter",
    resizeHeight: false,
    invert: false
  });
  console.log($('#pad').width());
  refresh();
});
